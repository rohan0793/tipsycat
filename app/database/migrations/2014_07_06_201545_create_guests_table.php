<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('venue_id')->unsigned();
			$table->foreign('venue_id')
			      ->references('id')->on('venues')
			      ->onDelete('cascade');
			$table->string('name');
			$table->bigInteger('contact')->unsigned();
			$table->enum('gender', ['male', 'female']);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guests');
	}

}
