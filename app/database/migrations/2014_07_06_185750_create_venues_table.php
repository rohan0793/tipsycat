<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venues', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')
			      ->references('id')->on('users')
			      ->onDelete('cascade');
			$table->bigInteger('contact')->unsigned();
			$table->integer('popularity')->unsigned()->default(0);
			$table->string('name');
			$table->longText('rules');
			$table->string('location');
			$table->string('address');
			$table->string('type');
			$table->time('timings');
			$table->string('pr');
			$table->enum('priority', ['free', 'paid', 'platinum'])->default('free');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venues');
	}

}
