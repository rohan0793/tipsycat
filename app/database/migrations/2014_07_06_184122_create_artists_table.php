<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('artists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')
			      ->references('id')->on('users')
			      ->onDelete('cascade');
			$table->bigInteger('contact')->unsigned();
			$table->integer('popularity')->unsigned()->default(0);
			$table->string('first_name');
			$table->string('last_name');
			$table->string('stage_name');
			$table->date('dob')->nullable();
			$table->string('genre');
			$table->enum('status', ['disabled', 'enabled'])->default('disabled');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('artists');
	}

}
