<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaylistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('playlists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('artist_id')->unsigned();
			$table->integer('event_id')->unsigned();
			$table->foreign('artist_id')
			      ->references('id')->on('artists')
			      ->onDelete('cascade');
			$table->foreign('event_id')
			      ->references('id')->on('events')
			      ->onDelete('cascade');
			$table->string('name');
			$table->date('date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('playlists');
	}

}
