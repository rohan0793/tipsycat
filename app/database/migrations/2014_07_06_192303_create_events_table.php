<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('venue_id')->unsigned();
			$table->foreign('venue_id')
			      ->references('id')->on('venues')
			      ->onDelete('cascade');
			$table->integer('artist_id')->unsigned();
			$table->foreign('artist_id')
			      ->references('id')->on('artists')
			      ->onDelete('cascade');      
			$table->string('name');
			$table->dateTime('date');
			$table->integer('popularity')->unsigned()->default(0);
			$table->integer('entry')->unsigned();
			$table->integer('cover')->unsigned()->nullable();
			$table->longText('description');
			$table->smallInteger('capacity');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
