<?php
 
class ArtistsTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('artists')->delete();
        Artist::create([
                'user_id' => 1,
                'contact' => 1234567890,
                'popularity' => 0,
                'first_name' => 'Rohan',
                'last_name' => 'Chhabra',
                'stage_name' => 'RC',
                'dob' => '1993-04-07',
                'genre' => 'House',
                'status' => 1,
            ]);

        Artist::create([
                'user_id' => 2,
                'contact' => 1234567890,
                'popularity' => 0,
                'first_name' => 'Rohan',
                'last_name' => 'Chhabra',
                'stage_name' => 'RC',
                'dob' => '1993-04-07',
                'genre' => 'House',
                'status' => 1,
            ]);

        $faker = Faker\Factory::create();
        $users = User::where('role', '=', 'artist')->get();
        $count = User::where('role', '=', 'artist')->count();
        //dd($count);
        $ids = array();
        foreach($users as $user)
        {
            $ids[] = $user->id;
        }
        foreach(range(1, $count) as $index)
        {
            Artist::create([
                'user_id' => array_pop($ids),
                'contact' => $faker->numberBetween($min = 1000000000, $max = 9999999999),
                'popularity' => 0,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'stage_name' => $faker->firstName,
                'dob' => date($format = 'Y-m-d'),
                'genre' => $faker->word,
                'status' => 1,
            ]);
        }
    }
 
}