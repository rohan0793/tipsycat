<?php
 
class EventsTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('events')->delete();
        $faker = Faker\Factory::create();
        foreach(range(1, 100) as $index)
        {
            Eventt::create([
            	'venue_id' => rand(1, 10),
                'artist_id' => rand(1, 10),
                'name' => $faker->word,
                'date' => date($format = 'Y-m-d'),
                'entry' => rand(500, 1000),
                'description' => $faker->text($maxNbChars = 200),
                'entry' => rand(500, 1000),
            ]);
        }
    }
}