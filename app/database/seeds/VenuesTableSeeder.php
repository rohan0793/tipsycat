<?php
 
class VenuesTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('venues')->delete();
        Venue::create([
                'user_id' => 3,
                'contact' => 1234567890,
                'popularity' => 0,
                'name' => 'Kitty Su',
                'location' => 'New Delhi',
                'address' => '123 Lexinton Street',
                'type' => 'Pub',
                'timings' => '7pm-11pm',
                'pr' => 'Rohan',
            ]);

        $faker = Faker\Factory::create();
        $users = User::where('role', '=', 'venue')->get();
        $count = User::where('role', '=', 'venue')->count();
        //dd($count);
        $ids = array();
        foreach($users as $user)
        {
            $ids[] = $user->id;
        }
        foreach(range(1, $count) as $index)
        {
            Venue::create([
                'user_id' => array_pop($ids),
                'contact' => $faker->numberBetween($min = 1000000000, $max = 9999999999),
                'popularity' => 0,
                'name' => $faker->firstName,
                'location' => $faker->city,
                'address' => $faker->streetAddress,
                'type' => 'Pub',
                'timings' => '7pm-11pm',
                'pr' => $faker->firstName,
            ]);
        }
    }
 
}