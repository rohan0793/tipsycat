<?php
 
class SongsTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('songs')->delete();
        $faker = Faker\Factory::create();
        foreach(range(1, 100) as $index)
        {
            Song::create([
                'playlist_id' => rand(1, 10),
                'name' => $faker->word,
                'link' => 'www.' . $faker->word . '.com',
            ]);
        }
    }
 
}