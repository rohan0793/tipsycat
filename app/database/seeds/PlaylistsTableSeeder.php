<?php
 
class PlaylistsTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('playlists')->delete();
        $faker = Faker\Factory::create();
        foreach(range(1, 100) as $index)
        {
            Playlist::create([
            	'artist_id' => rand(1, 10),
                'event_id' => rand(1, 10),
                'name' => $faker->word,
                'date' => date($format = 'Y-m-d'),
            ]);
        }
    }
}