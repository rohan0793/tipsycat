<?php
 
class UsersTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('users')->delete();
 
        User::create(array(
            'email' => 'rohan0793@gmail.com',
            'username' => 'rohan0793',
            'password' => Hash::make('admin12345'),
            'role' => 'artist'
        ));

        User::create(array(
            'email' => 'rohan0794@gmail.com',
            'username' => 'rohan0794',
            'password' => Hash::make('admin12345'),
            'role' => 'artist'
        ));

        User::create(array(
            'email' => 'rohan0795@gmail.com',
            'username' => 'rohan0795',
            'password' => Hash::make('admin12345'),
            'role' => 'venue'
        ));

        User::create(array(
            'email' => 'rohan0796@gmail.com',
            'username' => 'rohan0796',
            'password' => Hash::make('admin12345'),
            'role' => 'admin'
        ));

        $faker = Faker\Factory::create();

        foreach(range(1,100) as $index)
        {
            $roles = ['artist', 'admin', 'venue'];
            User::create([
                    'email' => $faker->unique()->email,
                    'username' => $faker->unique()->username,
                    'password' => Hash::make('admin12345'),
                    'role' => $roles[array_rand($roles, 1)]
                ]);
        }
    }
 
}