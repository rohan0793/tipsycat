<?php

class PlaylistsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index($artist_id)
	{
		if(!Input::has('p') || !Input::has('c'))
		{
			$playlists = Playlist::where('artist_id', '=', $artist_id)->take(10)->get();
		}
		else
		{
			$p = Input::get('p')-1;
			$c = Input::get('c');
			$m = $p*$c;

			$playlists = Playlist::where('artist_id', '=', $artist_id)->take($c)->skip($m)->get();
		}

		return Response::json([
				'status' => 'success',
				'data' => $playlists
			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		if(Playlist::canCreate($input['artist_id']))
		{
			$validator = Validator::make($input, [
					'artist_id' => 'required|numeric',
					'event_id' => 'required|numeric',
					'name' => 'required|max:30',
					'date' => 'required|date',
				]);

			if($validator->fails())
			{
				return Response::json([
					'status' => 'error',
					'message' => $validator->messages()
					]);
			}

			$playlist = new Playlist;
			$playlist->artist_id = $input['artist_id'];
			$playlist->event_id = $input['event_id'];
			$playlist->name = $input['name'];
			$playlist->date = $input['date'];
			$save_playlist = $playlist->save();

			if($save_playlist)
			{
				return Response::json([
						'status' => 'success',
						'data' => $playlist
					]);
			}
			else
			{
				return Response::json([
						'status' => 'error',
						//'message' => 'User creation failed'
					]);
			}
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$playlist = Playlist::find($id);

		if($playlist)
		{
			return Response::json([
					'status' => 'success',
					'data' => $playlist
				]);
		}
		else
		{
			return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(!$playlist = Playlist::find($id))
		{
			return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
		}
		
		if(Playlist::canUpdate($id))
		{
			$input = Input::all();

			$validator = Validator::make($input, [
					'event_id' => 'numeric|exists:events,id',
					'name' => 'max:30',
					'date' => 'date',
				]);

			if($validator->fails())
			{
				return Response::json([
					'status' => 'error',
					'message' => $validator->messages()
					]);
			}

			if($playlist = Playlist::find($id))
			{
				$playlist_inputs = ['name', 'date', 'event_id'];

				foreach ($playlist_inputs as $key => $value)
				{
					if(Input::has($value))
					{
						$playlist->{$value} = $input[$value];
					}
				}

				$success = $playlist->save();

				return Response::json([
					'status' => 'success',
					'data' => $playlist
				]);
			}
			else
			{
				return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
			}
			
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Playlist::canUpdate($id))
		{
			$playlist = Playlist::find($id);
			if($playlist)
			{
				$success = $playlist->delete();

				return Response::json([
							'status' => 'success'
						]);
			}
			else
			{
				return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
			}
			
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


}
