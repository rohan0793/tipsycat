<?php

class PhotosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		if (Request::is('artists/*'))
		{
		    $user = Artist::find($id)->user;

		    $photos = Photo::where('user_id', '=', $user->id)->get();

		    return Response::json([
		    		'status' => 'success',
		    		'data' => $photos
		    	]);
		}

		if (Request::is('venues/*'))
		{
		    $user = Venue::find($id)->user;

		    $photos = Photo::where('user_id', '=', $user->id)->get();

		    return Response::json([
		    		'status' => 'success',
		    		'data' => $photos
		    	]);
		}

		if (Request::is('events/*'))
		{
		    $user = Eventt::find($id)->user;

		    $photos = Photo::where('user_id', '=', $user->id)->get();

		    return Response::json([
		    		'status' => 'success',
		    		'data' => $photos
		    	]);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (!Input::hasFile('photo')
			|| (!Input::has('artist_id')
			 	&& !Input::has('venue_id') 
			 	&& !Input::has('event_id')))
		{
		    return Response::json([
		    		'status' => 'error',
		    	]);
		}

		if(Input::has('artist_id'))
		{
			$user = User::find(Artist::find(Input::get('artist_id'))->user_id);
			if(!Photo::canCreate($user))
			{
				return Response::json([
		    		'status' => 'error',
		    		'message' => 'Inapropriate permissions'
		    	]);
			}
		}

		if(Input::has('venue_id'))
		{
			$user = User::find(Venue::find(Input::get('venue_id'))->user_id);
			if(!Photo::canCreate($user))
			{
				return Response::json([
		    		'status' => 'error',
		    		'message' => 'Inapropriate permissions'
		    	]);
			}
		}

		if(Input::has('event_id'))
		{
			$user = User::find(Eventt::find(Input::get('event_id'))->user_id);
			if(!Photo::canCreate($user))
			{
				return Response::json([
		    		'status' => 'error',
		    		'message' => 'Inapropriate permissions'
		    	]);
			}
		}

		$image = Image::make(Input::file('photo'));

		if($image->width() < 548 && $image->height() < 548)
		{
			return Response::json([
		    		'status' => 'error',
		    		'message' => 'Photo dimensions not allowed'
		    	]);
		}

		$hash = sha1(time() . $user->id);
		$ext = Input::file('photo')->getClientOriginalExtension();
		$bs = 'bs_' . $hash . '.' . $ext;
		$rect = 'rect_' . $hash . '.' . $ext;
		$ss = 'ss_' . $hash . '.' . $ext;

		if(Input::has('artist_id'))
		{
			$image->crop(548,548)
				->save(public_path() . '/images/' . $bs)
				->crop(274,274)
				->save(public_path() . '/images/' . $ss);
		}
		else
		{
			$image->crop(548,548)
				->save(public_path() . '/images/' . $bs)
				->crop(548,274)
				->save(public_path() . '/images/' . $rect)
				->crop(274,274)
				->save(public_path() . '/images/' . $ss);
		}
		

		Photo::create(['user_id' => $user->id, 'photo' => $hash . '.' . $ext]);

		return Response::json([
		    		'status' => 'success',
		    		'data' => $hash . '.' . $ext
		    	]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
