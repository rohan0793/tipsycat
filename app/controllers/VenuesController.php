<?php

class VenuesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(!Input::has('p') || !Input::has('c'))
		{
			$venues = Venue::take(10)->get();
		}
		else
		{
			$p = Input::get('p')-1;
			$c = Input::get('c');
			$m = $p*$c;

			$venues = Venue::take($c)->skip($m)->get();
		}

		return Response::json([
				'status' => 'success',
				'data' => $venues
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$validator = Validator::make($input, [
				'username' => 'required|unique:users|alpha_dash',
				'email' => 'required|email|unique:users',
				'password' => 'required|min:7|confirmed',
				'contact' => 'required|numeric|digits:10',
				'name' => 'required|alpha',
				'location' => 'required|alpha',
				'address' => 'required',
				'type' => 'required|alpha',
				'timings' => 'required',
				'pr' => 'required|alpha',
			]);

		if($validator->fails())
		{
			return Response::json([
				'status' => 'error',
				'message' => $validator->messages()
				]);
		}

		$user = new User;
		$venue = new Venue;

		$user_inputs = ['username', 'email', 'password'];
		$venue_inputs = ['contact', 'name', 'rules', 'location', 'address', 'type', 'timings', 'pr'];
		$input['password'] = Hash::make($input['password']);

		foreach ($user_inputs as $key => $value) {
			if(Input::has($value))
			{
				$user->{$value} = $input[$value];
			}
		}

		foreach ($venue_inputs as $key => $value) {
			if(Input::has($value))
			{
				$venue->{$value} = $input[$value];
			}
		}

		$user->save();
		$venue->user_id = $user->id;
		$success = $venue->save();

		if($success)
		{
			return Response::json([
					'status' => 'success',
					'data' => [
						'user' => $user,
						'venue' => $venue
					]
				]);
		}
		else
		{
			return Response::json([
					'status' => 'error',
					//'message' => 'User creation failed'
				]);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$venue = Venue::find($id);

		if($venue)
		{
			return Response::json([
					'status' => 'success',
					'data' => $venue
				]);
		}
		else
		{
			return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(Venue::canUpdate($id))
		{
			$input = Input::all();

			$validator = Validator::make($input, [
				'username' => 'unique:users|alpha_dash',
				'email' => 'email|unique:users',
				'password' => 'min:7|confirmed',
				'contact' => 'numeric|digits:10',
				'name' => 'alpha',
				'location' => 'alpha',
				'address' => '',
				'type' => 'alpha',
				'timings' => '',
				'pr' => 'alpha',
			]);

			if($validator->fails())
			{
				return Response::json([
					'status' => 'error',
					'message' => $validator->messages()
					]);
			}

			if($venue = Venue::find($id) && $user = Venue::find($id)->user)
			{
				$user_inputs = ['username', 'email', 'password'];
				$venue_inputs = ['contact', 'name', 'rules', 'location', 'address', 'type', 'timings', 'pr'];

				if(Input::has('password'))
				{
					$input['password'] = Hash::make($input['password']);
				}

				foreach ($user_inputs as $key => $value) {
					if(Input::has($value))
					{
						$user->{$value} = $input[$value];
					}
				}

				foreach ($venue_inputs as $key => $value) {
					if(Input::has($value))
					{
						$user->venue->{$value} = $input[$value];
					}
				}

				$success = $user->push();

				return Response::json([
					'status' => 'success',
					'data' => [
						'user' => $user,
						'venue' => $user->venue
					]
				]);
			}
			else
			{
				return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
			}
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Venue::canUpdate($id))
		{
			if(Venue::find($id) && $user = Venue::find($id)->user)
			{
				$user->delete();

				return Response::json([
					'status' => 'success'
				]);
			}
			else
			{
				return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
			}
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


}
