<?php

class ArtistsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(!Input::has('p') || !Input::has('c'))
		{
			$artists = Artist::take(10)->get();
		}
		else
		{
			$p = Input::get('p')-1;
			$c = Input::get('c');
			$m = $p*$c;

			$artists = Artist::take($c)->skip($m)->get();
		}

		return Response::json([
				'status' => 'success',
				'data' => $artists
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$validator = Validator::make($input, [
				'username' => 'required|unique:users|alpha_dash',
				'email' => 'required|email|unique:users',
				'password' => 'required|min:7|confirmed',
				'contact' => 'required|numeric|digits:10',
				'first_name' => 'required|alpha',
				'last_name' => 'required|alpha',
				'stage_name' => 'required|alpha',
				'dob' => 'required|date',
				'genre' => 'required|alpha',
			]);

		if($validator->fails())
		{
			return Response::json([
				'status' => 'error',
				'message' => $validator->messages()
				]);
		}

		$user = new User;
		$artist = new Artist;

		$user->username = $input['username'];
		$user->email = $input['email'];
		$user->password = Hash::make($input['password']);
		$user->role = 'artist';
		$save_user = $user->save();
		$artist->user_id = $user->id;
		$artist->contact = $input['contact'];
		$artist->first_name = $input['first_name'];
		$artist->last_name = $input['last_name'];
		$artist->stage_name = $input['stage_name'];
		$artist->dob = $input['dob'];
		$artist->genre = $input['genre'];
		$save_artist = $artist->save();
		$save_artist = $artist->save();

		if($save_user && $save_artist)
		{
			return Response::json([
					'status' => 'success',
					'data' => [
						'user' => $user,
						'artist' => $artist
					]
				]);
		}
		else
		{
			return Response::json([
					'status' => 'error',
					//'message' => 'User creation failed'
				]);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$artist = Artist::find($id);

		if($artist)
		{
			return Response::json([
					'status' => 'success',
					'data' => $artist
				]);
		}
		else
		{
			return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(Artist::canUpdate($id))
		{
			$input = Input::all();

			$validator = Validator::make($input, [
					'username' => 'unique:users|alpha_dash',
					'email' => 'email|unique:users',
					'password' => 'min:7|confirmed',
					'contact' => 'numeric|digits:10',
					'first_name' => 'alpha',
					'last_name' => 'alpha',
					'stage_name' => 'alpha',
					'dob' => 'date',
				]);

			if($validator->fails())
			{
				return Response::json([
					'status' => 'error',
					'message' => $validator->messages()
					]);
			}

			if($artist = Artist::find($id) && $user = Artist::find($id)->user)
			{
				$user_inputs = ['username' => '', 'email' => '', 'password' => ''];
				$artist_inputs = ['contact' => '', 'first_name' => '', 'last_name' => '', 'stage_name' => '', 'dob' => ''];

				if(Input::has('password'))
				{
					$input['password'] = Hash::make($input['password']);
				}
				
				foreach ($user_inputs as $key => $value)
				{
					if(Input::has($key))
					{
						$user->{$key} = $input[$key];
					}
				}

				foreach ($artist_inputs as $key => $value)
				{
					if(Input::has($key))
					{
						$user->artist->{$key} = $input[$key];
					}
				}

				$success = $user->push();

				return Response::json([
					'status' => 'success',
					'data' => [
						'user' => $user,
						'artist' => $user->artist
					]
				]);
			}
			else
			{
				return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
			}
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Artist::canUpdate($id))
		{
			if(Artist::find($id) && $user = Artist::find($id)->user)
			{
				$user->delete();

				return Response::json([
					'status' => 'success'
				]);
			}
			else
			{
				return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
			}
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);

	}


}
