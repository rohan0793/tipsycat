<?php

class SongsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($playlist_id)
	{
		if(!Input::has('p') || !Input::has('c'))
		{
			$songs = Song::where('playlist_id', '=', $playlist_id)->take(10)->get();
		}
		else
		{
			$p = Input::get('p')-1;
			$c = Input::get('c');
			$m = $p*$c;

			$songs = Song::take($c)->skip($m)->get();
		}

		return Response::json([
				'status' => 'success',
				'data' => $songs
			]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		if(Song::canCreate($input['playlist_id']))
		{
			$validator = Validator::make($input, [
					'name' => 'required|max:30',
					'link' => 'required',
				]);

			if($validator->fails())
			{
				return Response::json([
					'status' => 'error',
					'message' => $validator->messages()
					]);
			}

			$song = new Song;
			$song->playlist_id = $input['playlist_id'];
			$song->name = $input['name'];
			$song->link = $input['link'];
			$save_song = $song->save();

			if($save_song)
			{
				return Response::json([
						'status' => 'success',
						'data' => $song
					]);
			}
			else
			{
				return Response::json([
						'status' => 'error',
						//'message' => 'User creation failed'
					]);
			}
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$song = Song::find($id);

		if($song)
		{
			return Response::json([
					'status' => 'success',
					'data' => $song
				]);
		}
		else
		{
			return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
		}
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(!$song = Song::find($id))
		{
			return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
		}
		
		if(Song::canUpdate($id))
		{
			$input = Input::all();

			$validator = Validator::make($input, [
					'name' => 'max:30',
					'link' => '',
				]);

			if($validator->fails())
			{
				return Response::json([
					'status' => 'error',
					'message' => $validator->messages()
					]);
			}

			if($song = Song::find($id))
			{
				$song_inputs = ['name' => '', 'link' => ''];

				foreach ($song_inputs as $key => $value)
				{
					if(Input::has($key))
					{
						$song->{$key} = $input[$key];
					}
				}

				$success = $song->save();

				return Response::json([
					'status' => 'success',
					'data' => $song
				]);
			}
			else
			{
				return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
			}
		}
		
		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Song::canUpdate($id))
		{
			$song = Song::find($id);
			if($song){
				$song->delete();
				
				return Response::json([
						'status' => 'success'
					]);
			}
			else
			{
				return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
			}
			
		}
				
		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


}
