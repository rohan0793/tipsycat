<?php

class EventsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($venue_id)
	{
		if(!Input::has('p') || !Input::has('c'))
		{
			$events = Eventt::where('venue_id', '=', $venue_id)->take(10)->get();
		}
		else
		{
			$p = Input::get('p')-1;
			$c = Input::get('c');
			$m = $p*$c;

			$events = Eventt::where('venue_id', '=', $venue_id)->take($c)->skip($m)->get();
		}

		return Response::json([
				'status' => 'success',
				'data' => $events
			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		if(Eventt::canCreate($input['venue_id']))
		{
			$validator = Validator::make($input, [
					'venue_id' => 'required|numeric',
					'artist_id' => 'required|numeric',
					'name' => 'required|alpha_num',
					'date' => 'required|date',
					'entry' => 'required|numeric',
					'cover' => 'numeric',
					'description' => 'required|max:5000',
					'capacity' => 'required|numeric',
				]);

			if($validator->fails())
			{
				return Response::json([
					'status' => 'error',
					'message' => $validator->messages()
					]);
			}

			$event = Eventt::create($input);

			return Response::json([
					'status' => 'success',
					'data' => $event
				]);
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$event = Eventt::find($id);

		if($event)
		{
			return Response::json([
					'status' => 'success',
					'data' => $event
				]);
		}
		else
		{
			return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(!$event = Eventt::find($id))
		{
			return Response::json([
					'status' => 'error',
					'message' => 'id does not exist'
				]);
		}

		if(Eventt::canUpdate($id))
		{
			$input = Input::all();

			$validator = Validator::make($input, [
					'artist_id' => 'numeric|exists:artists,id',
					'name' => 'alpha_num',
					'date' => 'date',
					'entry' => 'numeric',
					'cover' => 'numeric',
					'description' => 'max:5000',
					'capacity' => 'numeric',
				]);

			if($validator->fails())
			{
				return Response::json([
					'status' => 'error',
					'message' => $validator->messages()
					]);
			}

			$event_inputs = ['artist_id', 'name', 'date', 'capacity', 'cover', 'entry', 'description', 'artist_id'];

			foreach ($event_inputs as $key => $value)
			{
				if(Input::has($value))
				{
					$event->{$value} = $input[$value];
				}
			}

			$event->save();

			return Response::json([
				'status' => 'success',
				'data' => $event
			]);
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Eventt::canUpdate($id))
		{
			if($event = Eventt::find($id))
			{
				$event->delete();

				return Response::json([
						'status' => 'success'
					]);
			}
			else
			{
				return Response::json([
						'status' => 'error',
						'message' => 'id does not exist'
					]);
			}
		}

		return Response::json([
				'status' => 'error',
				'message' => 'Inapropriate permissions'
			]);
	}


}
