<?php

class SessionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::check())
		{
			return Response::json([
					'status' => 'success',
					'message' => 'User logged in'
				]);
		}
		else
		{
			return Response::json([
					'status' => 'error',
					'message' => 'User not logged in'
				]);
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$attempt = Auth::attempt([
			'email' => $input['email'],
			'password' => $input['password']
		]);

		if(!$attempt)
		{
			$attempt = Auth::attempt([
				'username' => $input['email'],
				'password' => $input['password']
			]);
		}

		if(!$attempt)
		{
			return Response::json([
					'status' => 'error',
					'message' => 'Login failed'
				]);
		}
		else
		{
			return Response::json([
					'status' => 'success',
					'message' => 'Login successful'
				]);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
		return Response::json([
				'status' => 'success',
			]);
	}


}
