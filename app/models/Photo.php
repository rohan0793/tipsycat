<?php

class Photo extends Eloquent {
	protected $table = 'photos';
	protected $fillable = ['user_id', 'photo'];

	public static function canCreate($user)
	{
		if(Auth::check())
		{
			if($user->id == Auth::user()->id)
			{
				return true;
			}
		}

		return false;
	}

}