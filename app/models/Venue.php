<?php
 
class Venue extends Eloquent {
 
    protected $table = 'venues';

    //protected $fillable = array('first_name', 'last_name');

    public function user()
    {
        return $this->belongsTo('User');
    }
    
    // Used to verify the user for both update and delete
    public static function canUpdate($venue_id)
    {
    	if(!Auth::check())
        {
            return false;
        }

        $user = Auth::user();

        if($user->role == 'admin')
        {
            return true;
        }

        if($user->role == 'venue')
        {
            $venue = User::find($user->id)->venue;
            if($venue->id == $venue_id)
            {
                return true;
            }
        }

        return false;
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'LIKE', "$search%");
    }
}