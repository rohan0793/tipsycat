<?php
 
class Playlist extends Eloquent {
 
    protected $table = 'playlists';

    public function artist()
    {
        return $this->belongsTo('Artist');
    }

    public static function canCreate($artist_id = NULL)
    {
        if(!Auth::check() || $artist_id == NULL)
        {
            return false;
        }

        $user = Auth::user();

        if($user->role == 'admin')
        {
            return true;
        }

        if($user->role == 'artist')
        {
            $artist = User::find($user->id)->artist;
            if($artist->id == $artist_id)
            {
                return true;
            }
        }

        return false;
    }

    // Used to verify the user for both update and delete
    public static function canUpdate($playlist_id)
    {
        if(!Auth::check())
        {
            return false;
        }

        $user = Auth::user();

        if($user->role == 'admin')
        {
            return true;
        }

        if($user->role == 'artist')
        {
            if(Playlist::find($playlist_id)->artist->id == User::find($user->id)->artist->id)
            {
                return true;
            }
        }

        return false;
    }
    
}