<?php
 
class Song extends Eloquent {
 
    protected $table = 'songs';

    public function playlist()
    {
        return $this->belongsTo('Playlist');
    }

    public static function canCreate($playlist_id)
    {
        if(!Auth::check() || $playlist_id == NULL)
        {
            return false;
        }

        $user = Auth::user();

        if($user->role == 'admin')
        {
            return true;
        }

        if($user->role == 'artist')
        {
            $playlist = Playlist::find($playlist_id);
            $artist = Artist::find($playlist->artist_id);
            $user = User::find($artist->user_id);
            if($user->id == Auth::id())
            {
                return true;
            }
        }

        return false;
    }
    public static function canUpdate($song_id = NULL)
    {
        if(!Auth::check() || $song_id == NULL)
        {
            return false;
        }

        $user = Auth::user();

        if($user->role == 'admin')
        {
            return true;
        }

        if($user->role == 'artist')
        {
            if(Song::find($song_id)->playlist->artist->user->id == Auth::id())
            {
                return true;
            }
        }

        return false;
    }
}