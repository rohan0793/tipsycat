<?php
 
class Eventt extends Eloquent {
 
    protected $table = 'events';

    protected $fillable = array('venue_id', 'artist_id', 'name', 'date', 'description', 'capacity', 'cover', 'entry');

    public function venue()
    {
        return $this->belongsTo('Venue');
    }

    public static function canCreate($venue_id = NULL)
    {
        if(!Auth::check() || $venue_id == NULL)
        {
            return false;
        }

        $user = Auth::user();

        if($user->role == 'admin')
        {
            return true;
        }

        if($user->role == 'venue')
        {
            $venue = User::find($user->id)->venue;
            if($venue->id == $venue_id)
            {
                return true;
            }
        }

        return false;
    }

    // Used to verify the user for both update and delete
    public static function canUpdate($event_id)
    {
        if(!Auth::check())
        {
            return false;
        }

        $user = Auth::user();

        if($user->role == 'admin')
        {
            return true;
        }

        if($user->role == 'venue')
        {
            if(Eventt::find($event_id)->venue->id == User::find($user->id)->venue->id)
            {
                return true;
            }
        }

        return false;
    }
    
    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'LIKE', "$search%");
    }
}