<?php
 
class Artist extends Eloquent {
 
    protected $table = 'artists';

    //protected $fillable = array('first_name', 'last_name');

    public function user()
    {
        return $this->belongsTo('User');
    }
    
    // Used to verify the user for both update and delete
    public static function canUpdate($artist_id)
    {
    	if(!Auth::check())
        {
            return false;
        }

        $user = Auth::user();

        if($user->role == 'admin')
        {
            return true;
        }

        if($user->role == 'artist')
        {
            $artist = User::find($user->id)->artist;
            if($artist->id == $artist_id)
            {
                return true;
            }
        }

        return false;
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('stage_name', 'LIKE', "$search%");
    }
}