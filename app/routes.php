<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//testing
Route::get('/', function(){
	/*$array = array();
	for ($i=0; $i < 50; $i++) { 
		$array[] = sha1(time());
	}
	return Response::json($array);*/

	//return Response::json(isset($user = User::find(Artist::find(20)->user_id)));
});

Route::resource('users', 'UsersController');


Route::resource('artists', 'ArtistsController');
Route::get('/artists/{artist_id}/playlists', ['uses' => 'PlaylistsController@index']);
Route::resource('playlists', 'PlaylistsController', ['except' => 'index']);
Route::get('/playlists/{playlist_id}/songs', ['uses' => 'SongsController@index']);
Route::resource('songs', 'SongsController', ['except' => 'index']);


Route::resource('venues', 'VenuesController');
Route::get('/venues/{venue_id}/events', ['uses' => 'EventsController@index']);
Route::resource('events', 'EventsController', ['except' => 'index']);

Route::get('login', 'SessionsController@index');
Route::post('login', 'SessionsController@store');
Route::get('logout', 'SessionsController@destroy');
Route::resource('sessions', 'SessionsController', ['only' => ['index', 'store', 'destroy']]);


Route::resource('photos', 'PhotosController', ['except' => 'index']);
Route::get('/artists/{artist_id}/photos', ['uses' => 'PhotosController@index']);
Route::get('/venues/{venue_id}/photos', ['uses' => 'PhotosController@index']);
Route::get('/events/{event_id}/photos', ['uses' => 'PhotosController@index']);

Route::get('/search/{search}', 'SearchController@index');